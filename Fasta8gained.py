
'''
Created on Oct 26, 2018

@author: Elliott
'''
## selct then tab to indent
#from Bio import motifs
#from Bio.motifs import_pwm
from Bio.Seq import Seq
import pandas as pd
from Bio.SeqRecord import SeqRecord
#from Bio.Alphabet import generic_dna
from Bio.Alphabet import IUPAC
from builtins import isinstance
#import Bio.Alphabet
## disable polling in preferences> network connections > news


class Fasta8gained(object):
    '''
    f8 = Fasta8gained(foreground_seq4, backgound_seq4)
    find motifs that are A conserved in all 4 species
    B) lost in 1-4 foreground species
    C) Gained int 2-4 foreground species
    uses thresholds defined by Dabroski et. al in 2015 BMC Bioinformatics
    to distinguish hitsvs not
    '''

    def __init__(self, foreground_fasta0, background_fasta0):
        '''
        Constructor
        '''
        self.foreground_fasta = foreground_fasta0
        self.background_fasta = background_fasta0
        self.df_out_lost = pd.DataFrame( {'AnimalsWithHits':[],'LostIn':[],'GainedIn':[], 'Position':[], "LostGained":[]})
        self.df_out_gained = pd.DataFrame( {'AnimalsWithHits':[],'LostIn':[],'GainedIn':[], 'Position':[], "LostGained":[]})
        self.hit_list_bg = []
        self.hit_list_fg = []
        self.hit_dict_bg = {}
        self.hit_dict_fg = {}
        self.score_dict_bg = {}
        self.score_dict_fg = {}
        self.record_fg = None
        self.record_fg = None
        self.bg_species = []
        self.foreground_species_list = []
        #self.threshold = None


    def get_foreground_fasta(self):
        return self.__foreground_fasta


    def get_background_fasta(self):
        return self.__background_fasta


    def get_df_out_lost(self):
        return self.__df_out_lost


    def get_threshold(self):
        return self.__threshold


    def get_pssm_0(self):
        return self.__pssm0


    def get_hit_list_bg(self):
        return self.__hit_list_bg


    def get_hit_list_fg(self):
        return self.__hit_list_fg


    def get_hit_dict_bg(self):
        return self.__hit_dict_bg


    def get_hit_dict_fg(self):
        return self.__hit_dict_fg


    def get_score_dict_bg(self):
        return self.__score_dict_bg


    def get_score_dict_fg(self):
        return self.__score_dict_fg


    def set_foreground_fasta(self, value):
        self.__foreground_fasta = value


    def set_background_fasta(self, value):
        self.__background_fasta = value


    def set_df_out_lost(self, value):
        self.__df_out_lost = value


    def set_threshold(self, value):
        self.__threshold = value


    def set_pssm_0(self, value):
        self.__pssm0 = value


    def set_hit_list_bg(self, value):
        self.__hit_list_bg = value


    def set_hit_list_fg(self, value):
        self.__hit_list_fg = value


    def set_hit_dict_bg(self, value):
        self.__hit_dict_bg = value


    def set_hit_dict_fg(self, value):
        self.__hit_dict_fg = value


    def set_score_dict_bg(self, value):
        self.__score_dict_bg = value


    def set_score_dict_fg(self, value):
        self.__score_dict_fg = value


    def del_foreground_fasta(self):
        del self.__foreground_fasta


    def del_background_fasta(self):
        del self.__background_fasta


    def del_df_out_lost(self):
        del self.__df_out_lost


    def del_threshold(self):
        del self.__threshold


    def del_pssm_0(self):
        del self.__pssm0


    def del_hit_list_bg(self):
        del self.__hit_list_bg


    def del_hit_list_fg(self):
        del self.__hit_list_fg


    def del_hit_dict_bg(self):
        del self.__hit_dict_bg


    def del_hit_dict_fg(self):
        del self.__hit_dict_fg


    def del_score_dict_bg(self):
        del self.__score_dict_bg


    def del_score_dict_fg(self):
        del self.__score_dict_fg

    ## this replaces gapps in the seqence (-) with N to keep everything aligned
    def gap_to_n(self):
        foreground_fasta00 = self.foreground_fasta
        foreground_fasta = []
        for rec_fg in foreground_fasta00:
            if not isinstance(rec_fg, (SeqRecord)):
                continue
            seq00 = str(rec_fg.seq).replace("-", "N")
            foreground_fasta.append(SeqRecord(Seq(seq00, IUPAC.IUPACUnambiguousDNA()), id = rec_fg.id))
        self.foreground_fasta = foreground_fasta

        background_fasta00 = self.background_fasta
        background_fasta = []
        for rec_bg in background_fasta00:
            if not isinstance(rec_bg, (SeqRecord)):
                continue
            seq0000 = str(rec_bg.seq).replace("-", "N")
            background_fasta.append(SeqRecord(Seq(seq0000, IUPAC.IUPACUnambiguousDNA()), id = rec_bg.id))
        self.background_fasta = background_fasta

        return self.background_fasta
    ## get list of motif sites for a given 50bp region
    def get_hit_list(self, pfm0):
        self.threshold = pfm0.get_threshold()
        self.pssm0 = pfm0.get_pssm()
        self.hit_list_bg = []
        self.hit_list_fg = []

        self.hit_dict_bg = {}
        self.hit_dict_fg = {}
        self.score_dict_bg = {}
        self.score_dict_fg = {}
        self.motif_width = pfm0.get_width()
        #position_bg = -1
        self.alpha = IUPAC.IUPACUnambiguousDNA()## to accomodate 'N's
        self.fg_species = []
        self.fg_seq_dict = {}
        self.bg_seq_dict = {}
        for record_bg in self.background_fasta:

            bg_species_string = record_bg.id.split("_")[0]
            rec_seq_str = str(record_bg.seq)
#             if not rec_seq_str.isalpha():
#                 continue
            self.record_bg = SeqRecord(Seq(rec_seq_str, self.alpha), id = record_bg.id)
            self.bg_seq_dict[bg_species_string] = record_bg.seq
            if not isinstance(self.record_bg.seq, Seq):
                continue
            for position_bg, score_bg in self.pssm0.search(self.record_bg.seq, threshold = self.threshold ):  ## self.record_bg.seq,
                #if len(position_bg) > 0:
                ## dictionary with animal ID as key
                self.hit_dict_bg[bg_species_string + "_" + str(position_bg)] = (position_bg)
                self.score_dict_bg[bg_species_string + "_" + str(position_bg)] = str(score_bg)
                self.hit_list_bg.append(position_bg)

        for record_fg in self.foreground_fasta:
            fg_species_string = record_fg.id.split("_")[0]
            self.fg_species.append(fg_species_string)
            rec_seq_str_fg = str(record_fg.seq)
#             if not rec_seq_str_fg.isalpha():
#                 continue
            self.fg_seq_dict[fg_species_string] = record_fg.seq
            self.record_fg = SeqRecord(Seq(rec_seq_str_fg, self.alpha), id = record_fg.id)
            if not isinstance(self.record_fg.seq, Seq):
                continue
            for position_fg, score_fg in self.pssm0.search(record_fg.seq, threshold = self.threshold):
                #if (position_fg) > -1:
                self.hit_dict_fg[fg_species_string + "_" + str(position_fg)] = (position_fg)  ##
                self.score_dict_fg[fg_species_string + "_" + str(position_fg)] = str(score_fg)
                self.hit_list_fg.append(position_fg)##

        return  self.hit_list_fg
        #return isinstance(self.record_fg.seq, Seq)
    ## how many motifs gained in the foreground species relative to the background species?
    def get_gained_motif_df(self, bg_species_list, fg_species_list):
        self.background_species_list = bg_species_list
        self.foreground_species_list = fg_species_list
        df_gained_list = []
        for hit0 in list(set(self.hit_list_fg)):
            #fg_species0 = list(set(self.fg_species))
            ## how many hits in bg species? looking for those with zero
            ## bg_hits = False
            df_gained0 = self.df_out_gained
            if not hit0 in self.hit_list_bg:
                fg_hits = 0

                #animals_with_hits = []
                #pssm.calculate(test_seq)
                ## if more than 2 then do socres for fg
                #if fg_hits >= 2:
                fg_species0 = []##
                score_string_fg = []
                score_dict_fg = {}
                #make list of animals with motifs gained
                for fg_animal0 in list(set(self.fg_species)):
                    key_fg = fg_animal0 + "_" + str(hit0)
                    if key_fg in list(self.hit_dict_fg.keys()):
                        fg_hits = fg_hits + 1
                        fg_species0.append(fg_animal0)  ## to be used if fg_hits  >= 2
                    fg_seq0 = self.fg_seq_dict[fg_animal0]
                    if hit0 >= 0: ## for forward
                        fg_seq_sub = fg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(fg_seq_sub)):
                            score_fg0 = "NA"
                            score_string_fg.append( score_fg0 )
                            score_dict_fg[fg_animal0] = score_fg0
                            continue
                        try:
                            score_fg0 = str(self.pssm0.calculate(fg_seq_sub) )
                            score_string_fg.append( str(self.pssm0.calculate(fg_seq_sub) ) )##[0]
                            score_dict_fg[fg_animal0] = str(self.pssm0.calculate(fg_seq_sub))
                        except:
                            score_fg0 = "NA"
                        score_string_fg.append( score_fg0 )
                        score_dict_fg[fg_animal0] = score_fg0
                        ## for reverse comlemtn hits
                    else:
                        fg_seq_sub_rc = fg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(fg_seq_sub_rc)):
                            score_fg0 = "NA"
                            score_string_fg.append( score_fg0 )
                            score_dict_fg[fg_animal0] = score_fg0
                            continue
                        try:
                            score_fg0 = str( self.pssm0.reverse_complement().calculate(fg_seq_sub_rc) )

                        except:
                            score_fg0 = "NA"
                        score_string_fg.append(score_fg0)
                        score_dict_fg[fg_animal0] =score_fg0
                            #score_string_fg.append( str( self.pssm0.reverse_complement().calculate(fg_seq_sub) ) ) ## subset numpy array?[0])

                if fg_hits >= 1:

                    score_string_bg = []
                    score_dict_bg0 = {}
                    for bg_animal0 in self.background_species_list:
                        bg_seq0 = self.bg_seq_dict[bg_animal0]
                        if hit0 >= 0: ## for forward hits

                            bg_seq_sub = bg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                            #if self.motif_width <= len(bg_seq_sub):
                            if "N" in list(str(bg_seq_sub)):
                                score_bg0 = "NA"
                            else:
                                try:
                                    score_bg0 = str(self.pssm0.calculate(bg_seq_sub) )
                                except:
                                    score_bg0 = "NA"
                            score_string_bg.append( score_bg0 )##[0]
                            score_dict_bg0[bg_animal0] = (score_bg0)
                        ## for reverse complement hits
                        else:

                            bg_seq_sub_rc = bg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                            #if self.motif_width <= len(bg_seq_sub_rc):
                            if "N" in list(str(bg_seq_sub_rc)):
                                score0_rc = "NA"
                            else:
                                try:
                                    score0_rc = str(self.pssm0.reverse_complement().calculate(bg_seq_sub_rc) )
                                except:
                                    score0_rc = "NA"
                            score_string_bg.append( score0_rc) ## subset numpy array?[0])
                            score_dict_bg0[bg_animal0] = score0_rc
                if fg_hits >= 1:
                ## gained in 4. Laura, you could modify this part depending on the number of species in the foreground
                    hit0000 = str( hit0 )
                    #if fg_hits == 4:
                    #    df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)],'LostIn':[0],'GainedIn':[4], 'Position':[hit0000],  "LostGained":["Gained"]})
                    ## gained In 3 Species
                    #if fg_hits == 3:
                    #    df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[0],'GainedIn':[3], 'Position':[hit0000], "LostGained":["Gained"]})
                    ## gained in 2 species
                    if fg_hits == 2:
                        df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[0],'GainedIn':[2], 'Position':[hit0000],  "LostGained":["Gained"]})
                    ## gained in 1 , added 11/21/18
                    if fg_hits == 1:
                        df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[0],'GainedIn':[1], 'Position':[hit0000],  "LostGained":["Gained"]})
    #                 ## gained in 1 species
    #                 if fg_hits == 1:
    #                     df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[1],'GainedIn':[0], 'Position':[hit0000],  'ScoreStringFG' :["_".join(score_string_fg)],'ScoreStringBG' :["_".join(score_string_bg)], "LostGained":["Lost"]})
    #                 ## gained in 0 species
    #                 if fg_hits == 0:
    #                     df_gained0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[0],'GainedIn':[0], 'Position':[hit0000],  'ScoreStringFG' :["_".join(score_string_fg)],'ScoreStringBG' :["_".join(score_string_bg)], "LostGained":["Lost"]})
                    for fg_species00 in self.foreground_species_list:
                        if not fg_species00 in list(score_dict_fg.keys()): continue
                        df_gained0[fg_species00] = pd.Series(score_dict_fg[fg_species00], index = df_gained0.index)
                    for bg_species00 in self.background_species_list:
                        if not bg_species00 in list(score_dict_bg0.keys()): continue
                        df_gained0[bg_species00] = pd.Series(score_dict_bg0[bg_species00], index = df_gained0.index)
                    df_gained_list.append(df_gained0)
        if len(df_gained_list)  > 0:
            self.df_out_gained = pd.concat(df_gained_list, sort = True)  ## 11/9 False
        #return score_string_fg
        return self.df_out_gained

    ## how many motifs are lost in the foreground species relative to background species?
    def get_lost_motif_df(self, bg_species_list, fg_species_list):
        self.background_species_list = bg_species_list
        self.foreground_species_list = fg_species_list
        #score_string_fg = [];score_string_bg = []
        #fg_hits = 0
#        conserved = []; lost1 = []; lost2 = []; lost3 = [];lost4 = [];
        #x00 = self.score_dict_fg
        df_lost_list = []
        for hit0 in list(set(self.hit_list_bg)):
            #fg_species0 = list(set(self.fg_species))
            bg_hits = 0
            ## how many hits in bg species?
            score_string_bg = []
            df_lost0 = self.df_out_lost
            for bg_animal_pos in self.hit_dict_bg.keys():
                bg_animal = bg_animal_pos.split("_")[0]
                if hit0 == self.hit_dict_bg[bg_animal_pos]:
                    bg_hits = bg_hits + 1
                    key_bg = bg_animal + "_" + str(hit0)
                    if key_bg in list(self.score_dict_bg.keys()):
                        score_string_bg.append(  str(self.score_dict_bg[key_bg])  )

            ## if there are 4 in background, now check foreground for loss
            if bg_hits == 2:
                fg_species0 = list(set(self.fg_species))

                fg_hits = 0
                #animals_with_hits = []
                score_string_fg = []
                score_dict_fg = {}
                #pssm.calculate(test_seq)
                for fg_animal0 in list(set(self.fg_species)):
                    key_fg = fg_animal0 + "_" + str(hit0)
                    if key_fg in list(self.hit_dict_fg.keys()):
                        fg_hits = fg_hits + 1
                        if fg_animal0 in fg_species0:
                            fg_species0.remove(fg_animal0)

                    ## make score string for all fg seqences
                    fg_seq0 = self.fg_seq_dict[fg_animal0]
                    if hit0 >= 0: ## for forward
                        fg_seq_sub = fg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(fg_seq_sub)):
                            score_fg0 = "NA"
                        else:
                            try:
                                score_fg0 = str(self.pssm0.calculate(fg_seq_sub) )
                            except:
                                score_fg0 = "NA"
                        score_string_fg.append( score_fg0)
                        score_dict_fg[fg_animal0] = score_fg0
                    ## for reverse comlemtn hits
                    else:
                        fg_seq_sub_2 = fg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(fg_seq_sub_2)):
                            score_fg2 = "NA"
                        else:
                            try:
                                score_fg2 = str(self.pssm0.calculate(fg_seq_sub_2.reverse_complement()) )##reverse_complement()
                            except:
                                score_fg2 = "NA"
                        score_string_fg.append( score_fg2) ## subset numpy array?[0])
                        score_dict_fg[fg_animal0] = score_fg2
                        #if score_fg2 ==NA:

                #self.bg_seq_dict
                score_dict_bg00 = {}
                score_string_bg00 = []
                for bg_species00 in self.background_species_list:
                    bg_seq0 = self.bg_seq_dict[bg_species00]
                    if hit0 >= 0: ## for forward
                        bg_seq_sub = bg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(bg_seq_sub)):
                            score00 = "NA"
                        else:
                            try:
                                score00 = str(self.pssm0.calculate(bg_seq_sub) )
                            except:
                                score00 = "NA"
                        score_string_bg00.append( score00 )##[0]
                        score_dict_bg00[bg_species00] = score00
                    ## for reverse comlemtn hits
                    else:
                        bg_seq_sub_2 = bg_seq0[hit0:(hit0 + self.motif_width)] ## cut out just the seqence for the motif
                        if "N" in list(str(bg_seq_sub_2)):
                            score00_rc = "NA"
                        else:
                            try:
                                score00_rc = str(self.pssm0.reverse_complement().calculate(bg_seq_sub_2) )
                            except:
                                score00_rc = "NA"
                        score_string_bg00.append( score00_rc ) ## subset numpy array?[0])
                        score_dict_bg00[bg_species00] = score00_rc
                if "NA" in score_string_bg00:
                    continue
                ## conserved in 8 speceis
                ## Laura, you could modify this part depending on the number of species in the foreground and background
                hit0000 = str(hit0 )
                if fg_hits == 2:
                    df_lost0 = pd.DataFrame( {'AnimalsWithHits': [""],'LostIn':["0"],'GainedIn':[0], 'Position':[hit0000], "LostGained":["Conserved"]})
                ## lost In 1 Species
                #if fg_hits == 3:
                #    df_lost0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[1],'GainedIn':[0], 'Position':[hit0000], "LostGained":["Lost"]})
                ## lost in 2 species
                #if fg_hits == 2:
                #    df_lost0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[2],'GainedIn':[0], 'Position':[hit0000], "LostGained":["Lost"]})
                ## lost in 3 species
                if fg_hits == 1:
                    df_lost0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[1],'GainedIn':[0], 'Position':[hit0000], "LostGained":["Lost"]})
                ## lost in 4 species
                if fg_hits == 0:
                    df_lost0 = pd.DataFrame( {'AnimalsWithHits': ["_".join(fg_species0)], 'LostIn':[2],'GainedIn':[0], 'Position':[hit0000], "LostGained":["Lost"]})
                ## ad column for each species   fg, then bg
                for fg_species00 in self.foreground_species_list:
                    if not fg_species00 in list(score_dict_fg.keys()): continue
                    df_lost0[fg_species00] = pd.Series(score_dict_fg[fg_species00], index = df_lost0.index)
                for bg_species000 in self.background_species_list:
                    if not bg_species000 in list(score_dict_bg00.keys()): continue
                    df_lost0[bg_species000] = pd.Series(score_dict_bg00[bg_species000], index = df_lost0.index)

                df_lost_list.append(df_lost0)
        if len(df_lost_list) > 0:
            self.df_out_lost = pd.concat(df_lost_list, sort = True)  ##, sort = False 11/9
        #return score_string_fg
        return self.__df_out_lost

    foreground_fasta = property(get_foreground_fasta, set_foreground_fasta, del_foreground_fasta, "foreground_fasta's docstring")
    background_fasta = property(get_background_fasta, set_background_fasta, del_background_fasta, "background_fasta's docstring")
    df_out_lost = property(get_df_out_lost, set_df_out_lost, del_df_out_lost, "df_out_lost's docstring")
    threshold = property(get_threshold, set_threshold, del_threshold, "threshold's docstring")
    pssm0 = property(get_pssm_0, set_pssm_0, del_pssm_0, "pssm0's docstring")
    hit_list_bg = property(get_hit_list_bg, set_hit_list_bg, del_hit_list_bg, "hit_list_bg's docstring")
    hit_list_fg = property(get_hit_list_fg, set_hit_list_fg, del_hit_list_fg, "hit_list_fg's docstring")
    hit_dict_bg = property(get_hit_dict_bg, set_hit_dict_bg, del_hit_dict_bg, "hit_dict_bg's docstring")
    hit_dict_fg = property(get_hit_dict_fg, set_hit_dict_fg, del_hit_dict_fg, "hit_dict_fg's docstring")
    score_dict_bg = property(get_score_dict_bg, set_score_dict_bg, del_score_dict_bg, "score_dict_bg's docstring")
    score_dict_fg = property(get_score_dict_fg, set_score_dict_fg, del_score_dict_fg, "score_dict_fg's docstring")
    df_out_lost = property(get_df_out_lost, set_df_out_lost, del_df_out_lost, "df_out_lost's docstring")
