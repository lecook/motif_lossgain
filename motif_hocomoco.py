#!~/bin/python3

## Author: Elliott Ferris Oct 25, 2018 with edits by Laura E Cook, Unversity of Melbourne, 27/02/2020
## Purpose: Find motifs lossed/gained in convergent species
## Last update: 02/03/2020

## Usage: motif_search_for_laura.py twar
## Run on spartan server: ssh lecook@spartan.hpc.unimelb.edu.au

########################################################################################################################
####-------------------------------------------------IMPORT MODULES-------------------------------------------------####
########################################################################################################################

import sys
import re

import multiprocessing as mp ## for parallelization

'''
Created on Oct 25, 2018
motif_search_for_laura.py <chromosome>
motif_search_for_laura.py chr19
@author: Elliott Ferris
'''
if __name__ == '__main__':
    pass
import os
os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/")

from PFM import PFM

# Edit this for number of species, defines gain and loss
from Fasta8gained import Fasta8gained

from Bio import motifs
#from Bio.motifs import _pwm
#from Bio.Seq import Seq
from Bio.Alphabet import Gapped, generic_dna
#from Bio.Alphabet import IUPAC
from Bio import SeqIO
import pandas as pd
import time
start_time = time.time()
col_order = [ "TWAR", "LostGained", "LostIn","GainedIn", "AnimalsWithHits", "TF", "Position", "Width", "PFM_FileName", "MaxScore", "MinScore", "Threshold", "tcyn", "sarHar1", "ailMel1", "clup"]

################################################################################################################
####-------------------------------------------------INPUTS-------------------------------------------------####
################################################################################################################


# Set working directory
os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/")


xls_jaspar = open("hocomocothresholds.xlsx", "rb")

## Name the columns in the Dabroski files
##head1 = ["database", "matrix", "TF", "optimal_threshold_BiasAway6", "optimal_threshold_BiasAway6.5", "optimal_threshold_BiasAway7", "optimal_threshold_BiasAway7.5",	"optimal_threshold_BiasAway8",	"optimal_threshold_BiasAway8.5",	"optimal_threshold_BiasAway9",	"optimal_threshold_BiasAway9.5", "optimal_threshold_BiasAway10"]

# Save to a dataframe
jaspar_df = pd.read_excel(xls_jaspar, sheet_name = 0)

## This file tells me which species are foreground and which are background
#os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/")
#plan = pd.read_csv("convergent_tfbs_plan.txt", sep = "\t")
## I extract the genus_species strings for the background and foreground animals
#background4 = list(plan.query('Background == True')["UCSC.db.name"])
#foreground4 = list(plan.query('Foreground == True')["UCSC.db.name"])

# Define which species are foreground and background
background4 = ['sarHar1', 'ailMel1']
foreground4 = ['clup', 'tcyn']##

# Load files with the position weight matricies
## Set working directory
os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/hocomoco_PWM/")

## Save PWM file names to a list
pfm_file_names = os.listdir(".")
#print(pfm_file_names)
# Select only thresholds for selex PWMs
#jaspar_df = jaspar_df.loc[jaspar_df['database'] == "jaspar"]


###########################################################################################################################
####-------------------------------------------------SEARCH FOR MOTIFS-------------------------------------------------####
###########################################################################################################################

# Define search motifs function
def serch_motifs(ii):
    list_of_dataframes = []

    fasta8 = Fasta8gained(seq_foreground4, seq_background4)

    ## Get appropriate thresholds from the Dabroski dataset
    matrix_id = jaspar_df.loc[ii,'tf_name']
    threshold_BA = jaspar_df.loc[ii,'threshold_balanced']

    ## Save PWMs files that are present in the threshold dataset
    pwf_fn = [fn0 for fn0 in pfm_file_names if fn0.startswith(matrix_id + ".")]
    #print(fn0)
    ## Save just the TF name to a variable
    #tf0 = pwf_fn[0].split("_")[1].split(".")[0]
    tf0 = pwf_fn[0].split(".")[0]

    # #tf0 = pwf_fn[0].split(".")[0]
    #
    ## Open each PWM file
    handle_jaspar = open(pwf_fn[0])

    # Read motifs
    motf_matrix0 = motifs.read(handle_jaspar, "jaspar")

    ## Set threshold from bio.motifs thresholds for searching for motifs in a fasta file
    pfm0 = PFM(motf_matrix0, threshold_BA, tf0, pwf_fn)

    ## Replace gaps (-) in the fasta sequence with N to keep everything properly aligned
    fasta8.gap_to_n()

    ## Get a list of motif sites for a given 50bp region
    fasta8.get_hit_list(pfm0)

    ## Find motifs that are lost
    df_lost0000 = fasta8.get_lost_motif_df(background4, foreground4)

    ## Find motifs that are gained
    df_gained0000 = fasta8.get_gained_motif_df(background4, foreground4)

    ## Save to columns
    df_lost0000['TWAR'] = pd.Series(twar, index = df_lost0000.index)
    #df_lost0000['Start'] = pd.Series(str(start0), index = df_lost0000.index)
    #df_lost0000['Stop'] = pd.Series(str(stop0), index = df_lost0000.index)

    df_lost0000['TF'] = pd.Series(tf0, index = df_lost0000.index)
    df_lost0000['Width'] = pd.Series(str(len(motf_matrix0)), index = df_lost0000.index)
    df_lost0000['MinScore'] = pd.Series(str(motf_matrix0.pssm.min), index = df_lost0000.index)

    df_lost0000['MaxScore'] = pd.Series(str(motf_matrix0.pssm.max), index = df_lost0000.index)
    df_lost0000['Threshold'] = pd.Series(str(threshold_BA), index = df_lost0000.index)
    df_lost0000['PFM_FileName'] = pd.Series(pwf_fn[0], index = df_lost0000.index)

    df_gained0000['TWAR'] = pd.Series(twar, index = df_gained0000.index)
    #df_gained0000['Start'] = pd.Series(str(start0), index = df_gained0000.index)
    #df_gained0000['Stop'] = pd.Series(str(stop0), index = df_gained0000.index)

    df_gained0000['TF'] = pd.Series(tf0, index = df_gained0000.index)
    df_gained0000['Width'] = pd.Series(str(len(motf_matrix0)), index = df_gained0000.index)
    df_gained0000['MinScore'] = pd.Series(str(motf_matrix0.pssm.min), index = df_gained0000.index)

    df_gained0000['MaxScore'] = pd.Series(str(motf_matrix0.pssm.max), index = df_gained0000.index)
    df_gained0000['Threshold'] = pd.Series(str(threshold_BA), index = df_gained0000.index)
    df_gained0000['PFM_FileName'] = pd.Series(pwf_fn[0], index = df_gained0000.index)

    ## Append lost and gained motifs to list_of_dataframes
    list_of_dataframes.append( df_lost0000 )
    list_of_dataframes.append( df_gained0000 )
    df_record0 = pd.concat(list_of_dataframes, sort = True)  #, sort = False 11/9

    return df_record0

###########################################################################################################################
####------------------------------------------------------OUTPUT-------------------------------------------------------####
###########################################################################################################################

# Write output to this file
fn_out = "motif_lossgain_4balancedhcmoco.txt"

# Define first and last species in the entries
eighth_animal = "tcyn" ## Last species in fasta entries
first_animal =  "ailMel1" ## First species in fasta entries

# Set working directory
os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/")

# Start counter at 0
jj = 0

## Empty arrays
seq_foreground4 = []; seq_background4 = []; list_of_dataframes2 = []

# Input fasta file called from command line
fasta_fn = "twars_" + sys.argv[1] + ".fa"

#fasta_fn = sys.argv[1]

first = True

# For each sequence in the multiFASTA file
for record in SeqIO.parse(fasta_fn, "fasta", Gapped(generic_dna, gap_char="-")):

    #print  isinstance(record.seq.alphabet, Gapped)

    # Increase counter for each sequence
    jj = jj + 1

    if record.id.startswith(first_animal):
        seq_foreground4 = []; seq_background4 = []

    # For species in the foreground:
    for foreground0 in foreground4:
        # If record id starts with species ID
        if record.id.startswith(foreground0):
            # Then append the record to the empty array seq_foreground4
            seq_foreground4.append(record)

    # For species in the background:
    for background0 in background4:
        # If record id starts with species ID
        if record.id.startswith(background0):
            # Then append the record to the empty array seq_background4
            seq_background4.append(record)

    # If not record id starts with eighth animal (sarHar1) - I think this stops the loop
    if not record.id.startswith(eighth_animal):
        #print(seq_background4)
        continue
    #print len(seq_foreground4), ; print len(seq_background4)
    # This checks that if you've got to the end of the TWAR and each species is present
    if record.id.startswith(eighth_animal) and len(background0) + len(foreground0) < 4:
        #seq_foreground4 = []; seq_background4 = []
        print("Non All 4")
        continue
        #print "--"
    record_split = record.id.split("_")
    twar = record_split[1]
    print(twar)
    #if chr0[:3] != "chr":
    #    continue
    #start0 = record_split[3]
    #stop0 = record_split[4]

    # Set working directory for PWMs
    os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/hocomoco_PWM/")

    # Set PWM file names to a list variable
    pfm_file_names = os.listdir(".")

    # Run in parallel
    # I don't really get this but I left it because I couldn't work out how to change it exactly.
    p00 = mp.Pool(1)

    # Run serch_motifs for all the motifs and save to a variable
    list_of_dataframes = p00.map(serch_motifs, jaspar_df.index, 1) ## serch_motifs() is the functions defined above

    # Set working directory for saving the output
    os.chdir("/data/projects/punim0586/lecook/motif_lossgain/code/motif_summaries_py/")

    #df_motif_out.to_csv(path_or_buf = fn_out, sep = "\t", header = True)

    # Append output data to the list_of_dataframes
    df_out_append = pd.concat(list_of_dataframes, sort = True)## False 11/9

    # Close pool files otherwise the program runs out of memory - I added this.
    p00.close()
    p00.join()

    if df_out_append.empty == True:
        continue
    df_out_append_ordered = df_out_append[col_order] ## reorder 11/9/18
    #print df_out_append.head()
    if first == True and sys.argv[1] == "1":
        ## write to file
        df_out_append_ordered.to_csv(path_or_buf=fn_out, sep="\t", header=True)
        first = False
    else:
        ## write to tile
        df_out_append_ordered.to_csv(path_or_buf = fn_out, sep = "\t", header = False, mode = "a")



# Run details:
min0 = (time.time() - start_time)/60
print("--- %s minutes --- %s" % (min0, str(jj/8.0)))
